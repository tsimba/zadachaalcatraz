//============================================================================
// Name        : main.cpp
// Author      : Kiril Stoilov
// Version     : 1.0
// Copyright   :
// Description : Solution to median calculation challenge for alcatraz.ai
//============================================================================
#include "Izvadka.h"

int main(void)
{
    Izvadka<int> izvadka;
    izvadka.addValue(1);
    izvadka.addValue(2);
    izvadka.addValue(2);
    izvadka.addValue(3);
    izvadka.addValue(5);
    izvadka.addValue(9);

    std::cout << "Values: ";
    izvadka.printValues();
    std::cout << "The median is: " << izvadka.median() << std::endl;

    return EXIT_SUCCESS;
}

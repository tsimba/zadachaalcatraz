/*
 * Izvadka.h
 *
 *  Created on: Feb 2, 2020
 *      Author: tsimba
 */

#ifndef IZVADKA_H_
#define IZVADKA_H_
#include <vector>
#include <iostream>
#include <algorithm>

template<typename T>
class Izvadka
{
public:
    //-----------------------------------------------------------------------------------
    Izvadka() { };

    //-----------------------------------------------------------------------------------
    virtual ~Izvadka() { };

    ///Represents the exception for taking the median of an empty list
    class median_of_empty_list_exception: public std::exception
    {
        virtual const char* what() const throw ()
        {
            return "Attempt to take the median of an empty list of numbers.  "
                    "The median of an empty list is undefined.";
        }
    };

    //-----------------------------------------------------------------------------------
    /// As taken from: https://stackoverflow.com/questions/1719070/what-is-the-right-approach-when-using-stl-container-for-median-calculation/
    /// Return the median of a sequence of numbers defined by the random
    /// access iterators begin and end.  The sequence must not be empty
    /// (median is undefined for an empty set).
    ///
    /// The numbers must be convertible to double.
    double median()
    {
        auto begin = data.begin();
        auto end = data.end();
        if (begin == end)
        {
            throw median_of_empty_list_exception();
        }
        size_t size = end - begin;
        size_t middle_index = size / 2;
        auto target = begin + middle_index;
        std::nth_element(begin, target, end);

        //Odd number of elements
        if (size % 2 != 0)
        {
            return *target;
        }
        //Even number of elements
        else
        {
            double a = *target;
            auto target_neighbor = target - 1;
            std::nth_element(begin, target_neighbor, end);
            return (a + *target_neighbor) / 2.0;
        }
    }

    //-----------------------------------------------------------------------------------
    void addValue(T val)
    {
        data.push_back(val);
    }
    
    //-----------------------------------------------------------------------------------
    void printValues()
    {
        for(auto& i : data)
            std::cout << i << " ";
        std::cout << std::endl;
    }

private:
    //-----------------------------------------------------------------------------------
    Izvadka(const Izvadka&);

    //-----------------------------------------------------------------------------------
    Izvadka& operator=(const Izvadka&);


    std::vector< T > data;
};

#endif /* IZVADKA_H_ */

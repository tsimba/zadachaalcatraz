#include "gtest/gtest.h"
#include "Izvadka.h"

TEST(izvadka_test, test1)
{
    //arrange
    Izvadka<int> izvadka;
    
    //assert
    EXPECT_THROW(izvadka.median(), Izvadka<int>::median_of_empty_list_exception);
}

TEST(izvadka_test, test2)
{
    //arrange
    Izvadka<int> izvadka;
    izvadka.addValue(1);
    izvadka.addValue(2);
    izvadka.addValue(2);
    izvadka.addValue(3);
    izvadka.addValue(5);
    izvadka.addValue(9);
    
    //assert
    EXPECT_EQ(izvadka.median(), 2.5);
}


TEST(izvadka_test, test3)
{
    //arrange
    Izvadka<int> izvadka;
    izvadka.addValue(-9);
    izvadka.addValue(-5);
    izvadka.addValue(-3);
    izvadka.addValue(-2);
    izvadka.addValue(-2);
    izvadka.addValue(-9);
    izvadka.addValue(-10);

    //assert
    EXPECT_EQ(izvadka.median(), -5);
}

TEST(izvadka_test, test4)
{
    //arrange
    Izvadka<int> izvadka;
    izvadka.addValue(-1);
    izvadka.addValue(1);
    izvadka.addValue(-1);
    izvadka.addValue(1);
    izvadka.addValue(-1);
    izvadka.addValue(1);
    izvadka.addValue(-1);

    //assert
    EXPECT_EQ(izvadka.median(), -1);
}

TEST(izvadka_test, test5)
{
    //arrange
    Izvadka<int> izvadka;
    izvadka.addValue(1);
    izvadka.addValue(-1);
    izvadka.addValue(1);
    izvadka.addValue(-1);
    izvadka.addValue(1);
    izvadka.addValue(-1);
    izvadka.addValue(1);
    izvadka.addValue(-1);
    izvadka.addValue(1);

    //assert
    EXPECT_EQ(izvadka.median(), 1);
}

TEST(izvadka_test, test6)
{
    //arrange
    Izvadka<float> izvadka;
    izvadka.addValue(1.0);
    izvadka.addValue(-3.14);
    izvadka.addValue(20.1);
    izvadka.addValue(7.5);
    izvadka.addValue(33.3);
    izvadka.addValue(-255);
    izvadka.addValue(0.1);
    izvadka.addValue(17.0);

    //assert
    EXPECT_EQ(izvadka.median(), 4.25);
}

TEST(izvadka_test, test7)
{
    //arrange
    Izvadka<double> izvadka;
    izvadka.addValue(1.0000001);
    izvadka.addValue(-3.141592);
    izvadka.addValue(20.110011);
    izvadka.addValue(7.5432198);
    izvadka.addValue(33.333333);
    izvadka.addValue(-255);
    izvadka.addValue(0.0000001);
    izvadka.addValue(17.0);

    //assert
    const double result_internal = izvadka.median();
    const double result_external = 4.27160995l;
    const double error_margin = 0.0000001l;
    EXPECT_LT(result_internal - result_external, error_margin);
}











